#include "one_wire.h"

int main(void)
{
    stdio_init_all();
    setup_default_uart();
    gpio_init(PICO_DEFAULT_LED_PIN);
    gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);

    uint sm = one_wire_init(pio0, 15);
    uint8_t command[] = {0x33};
    uint8_t code[8] = {0};

    while (true)
    {
        gpio_put(PICO_DEFAULT_LED_PIN, 1);
        sleep_ms(500);
        gpio_put(PICO_DEFAULT_LED_PIN, 0);
        sleep_ms(500);
        write_byte(pio0, sm, command, sizeof(command));
        read_byte(pio0, sm, code, sizeof(code));
        printf("Code: %02X %02X %02X %02X %02X %02X %02X %02X\r\n", code[0], code[1], code[2], code[3], code[4], code[5], code[6], code[7]);
        sleep_ms(500);
    }
    return 0;
}