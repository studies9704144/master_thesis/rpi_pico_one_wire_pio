#ifndef ONE_WIRE_H
#define ONE_WIRE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/dma.h"
#include "hardware/pio.h"

#include "one_wire.pio.h"

void write_byte(PIO pio, uint sm, uint8_t bytes[], int len);

void read_byte(PIO pio, uint sm, uint8_t bytes[], int len);

uint one_wire_init(PIO pio, int gpio);

#endif